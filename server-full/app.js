const express = require('express');
const cors = require('cors');
const graphQLHTTP = require('express-graphql');
const mongoose = require('mongoose');
const schema = require('./schema');

const app = express();
const PORT = 4000;

mongoose.connect('mongodb://root:root@localhost:32768/testDB', {
  authSource: 'admin',
  useNewUrlParser: true,
});

mongoose.connection.once('open', () => console.log('Mongo is connected'));

app.get('/', (req, res) => res.send('<h3>Hello from express</h3>'));

app.use(cors());

app.use(
  '/graphql',
  graphQLHTTP({
    schema,
    graphiql: true,
  }),
);

app.listen(PORT, () => console.log(`Express is listening on port ${PORT}`));
