// root
import React from 'react';
import { ApolloProvider } from 'react-apollo';
import ApolloClient from 'apollo-boost';

// components
import BookList from './components/BookList';
import AddBook from './components/AddBook';

const client = new ApolloClient({
  uri: 'http://localhost:4000/graphql',
});

const App = () => (
  <ApolloProvider client={client}>
    <div id="main">
      <h1>Reading list</h1>
      <BookList />
      <AddBook />
    </div>
  </ApolloProvider>
);

export default App;
