// dummy data
const books = [
  { name: 'Book1', genre: 'Fantsy', id: '1', authorId: '1' },
  { name: 'Book2', genre: 'Fantsy', id: '2', authorId: '2' },
  { name: 'Book3', genre: 'Sci-Fi', id: '3', authorId: '3' },
  { name: 'Book4', genre: 'Sci-Fi', id: '4', authorId: '2' },
  { name: 'Book5', genre: 'Fantsy', id: '5', authorId: '3' },
  { name: 'Book6', genre: 'Sci-Fi', id: '6', authorId: '1' },
];

const authors = [
  { name: 'Author 1', age: 62, id: '1' },
  { name: 'Author 2', age: 50, id: '2' },
  { name: 'Author 3', age: 48, id: '3' },
];

module.exports = { books, authors };
