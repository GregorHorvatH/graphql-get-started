// core
import React, { Component } from 'react';
import { graphql } from 'react-apollo';

// components
import BookDetails from './BookDetails';

// queries
import { getBooksQuery } from '../queries';

class BookList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selected: null,
    };
  }

  displayBooks() {
    const { loading, books } = this.props.data;

    return loading ? (
      <div>Loading...</div>
    ) : (
      books.map(book => (
        <li
          key={book.id}
          onClick={e => {
            this.setState({
              selected: book.id,
            });
          }}
        >
          {book.name}
        </li>
      ))
    );
  }

  render() {
    const { selected } = this.state;

    return (
      <div>
        <ul id="book-list">{this.displayBooks()}</ul>
        <BookDetails bookId={selected} />
      </div>
    );
  }
}

export default graphql(getBooksQuery)(BookList);
