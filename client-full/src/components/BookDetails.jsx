// core
import React, { Component } from 'react';
import { graphql } from 'react-apollo';

// queries
import { getBookQuery } from '../queries';

class BookDetails extends Component {
  displayBookDetails() {
    const { book } = this.props.data;

    if (book) {
      return (
        <div id="book-detail">
          <h2>{book.name}</h2>
          <p>{book.genre}</p>
          <p>{book.author.name}</p>
          <p>{book.author.age}</p>
          <p>All books by this author:</p>
          <ul className="other-books">
            {book.author.books.map(item => (
              <li key={item.id}>{item.name}</li>
            ))}
          </ul>
        </div>
      );
    } else {
      return <p>No book selected...</p>;
    }
  }

  render() {
    return <div id="book-details">{this.displayBookDetails()}</div>;
  }
}

export default graphql(getBookQuery, {
  options: props => ({
    variables: { id: props.bookId },
  }),
})(BookDetails);
