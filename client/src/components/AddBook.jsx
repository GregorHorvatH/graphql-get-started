// core
import React, { Component } from 'react';

class AddBook extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      genre: '',
      authorId: '',
    };
  }

  displayAuthors() {
    const { loading, authors } = {
      loading: true,
      authors: null,
    };

    return loading ? (
      <option disabled>Loading...</option>
    ) : (
      authors.map(author => (
        <option key={author.id} value={author.id}>
          {author.name}
        </option>
      ))
    );
  }

  submitForm = e => {
    e.preventDefault();

    console.log(this.state);

    this.setState({
      name: '',
      genre: '',
      authorId: '',
    });
  };

  render() {
    const { name, genre, authorId } = this.state;

    return (
      <form id="add-book" onSubmit={this.submitForm}>
        <div className="field">
          <label>Book name: </label>
          <input
            type="text"
            onChange={e =>
              this.setState({
                name: e.target.value,
              })
            }
            value={name}
          />
        </div>

        <div className="field">
          <label>Genre: </label>
          <input
            type="text"
            onChange={e =>
              this.setState({
                genre: e.target.value,
              })
            }
            value={genre}
          />
        </div>

        <div className="field">
          <label>Author: </label>
          <select
            onChange={e =>
              this.setState({
                authorId: e.target.value,
              })
            }
            value={authorId}
          >
            <option>Select author</option>
            {this.displayAuthors()}
          </select>
        </div>

        <button>+</button>
      </form>
    );
  }
}

export default AddBook;
