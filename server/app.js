const express = require('express');
const cors = require('cors');

const app = express();
const PORT = 4000;

app.get('/', (req, res) => res.send('<h3>Hello from express</h3>'));

app.use(cors());

app.listen(PORT, () => console.log(`Express is listening on port ${PORT}`));
