// core
import React, { Component } from 'react';

// components
import BookDetails from './BookDetails';

class BookList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selected: null,
    };
  }

  displayBooks() {
    const { loading, books } = {
      loading: true,
      books: null,
    };

    return loading ? (
      <div>Loading...</div>
    ) : (
      books.map(book => (
        <li
          key={book.id}
          onClick={e => {
            this.setState({
              selected: book.id,
            });
          }}
        >
          {book.name}
        </li>
      ))
    );
  }

  render() {
    const { selected } = this.state;

    return (
      <div>
        <ul id="book-list">{this.displayBooks()}</ul>
        <BookDetails bookId={selected} />
      </div>
    );
  }
}

export default BookList;
